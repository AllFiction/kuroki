<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Vehiculo.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:32:04pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Vehiculo extends Model
{
	
	
    public $timestamps = false;
    
    protected $table = 'vehiculos';

	
}
