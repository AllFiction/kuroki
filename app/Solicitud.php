<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Solicitud.
 *
 * @author  The scaffold-interface created at 2020-09-24 08:27:30pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Solicitud extends Model
{
	
	
    protected $table = 'solicituds';

	
	public function vehiculo()
	{
		return $this->belongsTo('App\Vehiculo','vehiculo_id');
	}

	
}
