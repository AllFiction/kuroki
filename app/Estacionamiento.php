<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estacionamiento.
 *
 * @author  The scaffold-interface created at 2020-09-20 08:18:03pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Estacionamiento extends Model
{
	
	
    protected $table = 'estacionamientos';

	
	public function vatet()
	{
		return $this->belongsTo('App\Vatet','vatet_id');
	}

	
	public function vehiculo()
	{
		return $this->belongsTo('App\Vehiculo','vehiculo_id');
	}

	
}
