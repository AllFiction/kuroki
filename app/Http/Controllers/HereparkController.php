<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Herepark;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Valet;


use App\Vehiculo;


/**
 * Class HereparkController.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:38:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class HereparkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - herepark';
        $hereparks = Herepark::paginate(6);
        return view('herepark.index',compact('hereparks','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - herepark';
        
        $valets = Valet::all()->pluck('Rut','id');
        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');
        
        return view('herepark.create',compact('title','valets' , 'vehiculos'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $herepark = new Herepark();

        
        $herepark->Numero = $request->Numero;

        
        $herepark->Lugar = $request->Lugar;

        
        
        $herepark->valet_id = $request->valet_id;

        
        $herepark->vehiculo_id = $request->vehiculo_id;

        
        $herepark->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new herepark has been created !!']);

        return redirect('herepark');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - herepark';

        if($request->ajax())
        {
            return URL::to('herepark/'.$id);
        }

        $herepark = Herepark::findOrfail($id);
        return view('herepark.show',compact('title','herepark'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - herepark';
        if($request->ajax())
        {
            return URL::to('herepark/'. $id . '/edit');
        }

        
        $valets = Valet::all()->pluck('Rut','id');

        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');

        
        $herepark = Herepark::findOrfail($id);
        return view('herepark.edit',compact('title','herepark' ,'valets', 'vehiculos' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $herepark = Herepark::findOrfail($id);
    	
        $herepark->Numero = $request->Numero;
        
        $herepark->Lugar = $request->Lugar;
        
        
        $herepark->valet_id = $request->valet_id;

        
        $herepark->vehiculo_id = $request->vehiculo_id;

        
        $herepark->save();

        return redirect('herepark');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Warning!!','Would you like to remove This?','/herepark/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$herepark = Herepark::findOrfail($id);
     	$herepark->delete();
        return URL::to('herepark');
    }
}
