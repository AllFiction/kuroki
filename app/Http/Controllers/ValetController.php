<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Valet;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class ValetController.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:30:18pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class ValetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - valet';
        $valets = Valet::paginate(6);
        return view('valet.index',compact('valets','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - valet';
        
        return view('valet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $valet = new Valet();

        
        $valet->Rut = $request->Rut;

        
        $valet->Nombre = $request->Nombre;

        
        $valet->Contacto = $request->Contacto;

        
        
        $valet->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new valet has been created !!']);

        return redirect('valet');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - valet';

        if($request->ajax())
        {
            return URL::to('valet/'.$id);
        }

        $valet = Valet::findOrfail($id);
        return view('valet.show',compact('title','valet'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - valet';
        if($request->ajax())
        {
            return URL::to('valet/'. $id . '/edit');
        }

        
        $valet = Valet::findOrfail($id);
        return view('valet.edit',compact('title','valet'  ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $valet = Valet::findOrfail($id);
    	
        $valet->Rut = $request->Rut;
        
        $valet->Nombre = $request->Nombre;
        
        $valet->Contacto = $request->Contacto;
        
        
        $valet->save();

        return redirect('valet');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Warning!!','Would you like to remove This?','/valet/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$valet = Valet::findOrfail($id);
     	$valet->delete();
        return URL::to('valet');
    }
}
