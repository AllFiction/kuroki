<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Solicitud;
use Amranidev\Ajaxis\Ajaxis;
use URL;

use App\Vehiculo;


/**
 * Class SolicitudController.
 *
 * @author  The scaffold-interface created at 2020-09-24 08:27:31pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Index - solicitud';
        $solicituds = Solicitud::paginate(6);
        return view('solicitud.index',compact('solicituds','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create - solicitud';
        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');
        
        return view('solicitud.create',compact('title','vehiculos'  ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitud = new Solicitud();

        
        
        $solicitud->vehiculo_id = $request->vehiculo_id;

        
        $solicitud->save();

        $pusher = App::make('pusher');

        //default pusher notification.
        //by default channel=test-channel,event=test-event
        //Here is a pusher notification example when you create a new resource in storage.
        //you can modify anything you want or use it wherever.
        $pusher->trigger('test-channel',
                         'test-event',
                        ['message' => 'A new solicitud has been created !!']);

        return redirect('solicitud');
    }

    /**
     * Display the specified resource.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $title = 'Show - solicitud';

        if($request->ajax())
        {
            return URL::to('solicitud/'.$id);
        }

        $solicitud = Solicitud::findOrfail($id);
        return view('solicitud.show',compact('title','solicitud'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id,Request $request)
    {
        $title = 'Edit - solicitud';
        if($request->ajax())
        {
            return URL::to('solicitud/'. $id . '/edit');
        }

        
        $vehiculos = Vehiculo::all()->pluck('Patente','id');

        
        $solicitud = Solicitud::findOrfail($id);
        return view('solicitud.edit',compact('title','solicitud' ,'vehiculos' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $solicitud = Solicitud::findOrfail($id);
    	
        
        $solicitud->vehiculo_id = $request->vehiculo_id;

        
        $solicitud->save();

        return redirect('solicitud');
    }

    /**
     * Delete confirmation message by Ajaxis.
     *
     * @link      https://github.com/amranidev/ajaxis
     * @param    \Illuminate\Http\Request  $request
     * @return  String
     */
    public function DeleteMsg($id,Request $request)
    {
        $msg = Ajaxis::BtDeleting('Warning!!','Would you like to remove This?','/solicitud/'. $id . '/delete');

        if($request->ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$solicitud = Solicitud::findOrfail($id);
     	$solicitud->delete();
        return URL::to('solicitud');
    }
}
