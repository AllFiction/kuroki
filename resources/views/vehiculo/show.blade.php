@extends('scaffold-interface.layouts.appTest')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        Informacion del vehiculo
    </h1>
    <br>
    <a href='{!!url("vehiculo")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i> Registro de vehiculos</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Datos</th>
            <th>Informacion</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>Patente</b> </td>
                <td>{!!$vehiculo->Patente!!}</td>
            </tr>
            <tr>
                <td> <b>Marca</b> </td>
                <td>{!!$vehiculo->Marca!!}</td>
            </tr>
            <tr>
                <td> <b>Modelo</b> </td>
                <td>{!!$vehiculo->Modelo!!}</td>
            </tr>
            <tr>
                <td> <b>Comentario</b> </td>
                <td>{!!$vehiculo->Comentario!!}</td>
            </tr>
            <tr>
                <td> <b>Cliente</b> </td>
                <td>{!!$vehiculo->Nombre!!}</td>
            </tr>
            <tr>
                <td> <b>Contacto</b> </td>
                <td>{!!$vehiculo->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection
