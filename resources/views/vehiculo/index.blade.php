@extends('scaffold-interface.layouts.appTest')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        Listado De vehiculos
    </h1>
    <a href='{!!url("vehiculo")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> Registar vehiculo</a>
    <br>
    <br>
    <div class="box-body">
                    <div class="table-responsive">
                        <table class="display" cellspacing="0" width="100%" id = "test">
                          <thead>
                              <th>Patente</th>
                              <th>Marca</th>
                              <th>Modelo</th>
                              <th>Comentario</th>
                              <th>Cliente</th>
                              <th>Contacto</th>
                              <th>actions</th>
                          </thead>
                          <tbody>
                              @foreach($vehiculos as $vehiculo)
                              <tr>
                                  <td>{!!$vehiculo->Patente!!}</td>
                                  <td>{!!$vehiculo->Marca!!}</td>
                                  <td>{!!$vehiculo->Modelo!!}</td>
                                  <td>{!!$vehiculo->Comentario!!}</td>
                                  <td>{!!$vehiculo->Nombre!!}</td>
                                  <td>{!!$vehiculo->Contacto!!}</td>
                                  <td>
                                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/vehiculo/{!!$vehiculo->id!!}/deleteMsg" ><i class = 'fa fa-trash'> delete</i></a>
                                      <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/vehiculo/{!!$vehiculo->id!!}/edit'><i class = 'fa fa-edit'> edit</i></a>
                                      <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/vehiculo/{!!$vehiculo->id!!}'><i class = 'fa fa-eye'> info</i></a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
    {!! $vehiculos->render() !!}

</section>
@endsection
