@extends('scaffold-interface.layouts.app')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        Edit solicitud
    </h1>
    <a href="{!!url('solicitud')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> Solicitud Index</a>
    <br>
    <form method = 'POST' action = '{!! url("solicitud")!!}/{!!$solicitud->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label>vehiculos Select</label>
            <select name = 'vehiculo_id' class = "form-control">
                @foreach($vehiculos as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> Actualizar</button>
    </form>
</section>
@endsection
