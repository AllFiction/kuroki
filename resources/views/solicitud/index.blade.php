@extends('scaffold-interface.layouts.appTest')
@section('title','Index')
@section('content')

<section class="content">
    <h1>
        Solicitudes
    </h1>
    <a href='{!!url("solicitud")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> New</a>
    <br>
    <br>
    <table id="test" class = "table table-responsive" style = 'background:#fff'>
        <thead>
            <th>Patente</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>Fecha</th>
            <th>actions</th>
        </thead>
        <tbody>
            @foreach($solicituds as $solicitud)
            <tr>
                <td>{!!$solicitud->vehiculo->Patente!!}</td>
                <td>{!!$solicitud->vehiculo->Nombre!!}</td>
                <td>{!!$solicitud->vehiculo->Contacto!!}</td>
                <td>{!!$solicitud->created_at!!}</td>

                <td>
                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/solicitud/{!!$solicitud->id!!}/deleteMsg" ><i class = 'fa fa-trash'> delete</i></a>
                    <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/solicitud/{!!$solicitud->id!!}/edit'><i class = 'fa fa-edit'> edit</i></a>
                    <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/solicitud/{!!$solicitud->id!!}'><i class = 'fa fa-eye'> info</i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $solicituds->render() !!}

</section>
@endsection
