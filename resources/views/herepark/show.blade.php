@extends('scaffold-interface.layouts.appTest')
@section('title','Show')
@section('content')

<section class="content">
    <h1>
        herepark
    </h1>
    <br>
    <a href='{!!url("herepark")!!}' class = 'btn btn-primary'><i class="fa fa-home"></i>Listado Estacionamiento</a>
    <br>
    <table class = 'table table-bordered'>
        <thead>
            <th>Datos</th>
            <th>Informacion</th>
        </thead>
        <tbody>
            <tr>
                <td> <b>Numero</b> </td>
                <td>{!!$herepark->Numero!!}</td>
            </tr>
            <tr>
                <td> <b>Lugar</b> </td>
                <td>{!!$herepark->Lugar!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Rut Valet : </i></b>
                </td>
                <td>{!!$herepark->valet->Rut!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Nombre Valet : </i></b>
                </td>
                <td>{!!$herepark->valet->Nombre!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Contacto : </i></b>
                </td>
                <td>{!!$herepark->valet->Contacto!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Patente : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Patente!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Marca : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Marca!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Modelo : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Modelo!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Comentario : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Comentario!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Cliente : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Nombre!!}</td>
            </tr>
            <tr>
                <td>
                    <b><i>Contacto : </i></b>
                </td>
                <td>{!!$herepark->vehiculo->Contacto!!}</td>
            </tr>
        </tbody>
    </table>
</section>
@endsection
