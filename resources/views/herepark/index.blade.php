@extends('scaffold-interface.layouts.appTest')
@section('title','Index')
@section('content')

<section class="content">


    <h1>
        Herepark
    </h1>
    <a href='{!!url("herepark")!!}/create' class = 'btn btn-success'><i class="fa fa-plus"></i> Agregar</a>
    <br>
    <br>
    <div class="box-body">
                    <div class="table-responsive">
                        <table class="display" cellspacing="0" width="100%" id = "test">
                          <thead>
                              <th>N°</th>
                              <th>lugar</th>
                              <th>Patante</th>
                              <th>Contacto</th>
                              <th></th>
                          </thead>
                          <tbody>
                              @foreach($hereparks as $herepark)
                              <tr>
                                  <td>{!!$herepark->Numero!!}</td>
                                  <td>{!!$herepark->Lugar!!}</td>
                                  <td>{!!$herepark->vehiculo->Patente!!}</td>
                                  <td>{!!$herepark->vehiculo->Contacto!!}</td>
                                  <td>
                                    <a data-toggle="modal" data-target="#myModal" class = 'delete btn btn-danger btn-xs' data-link = "/herepark/{!!$herepark->id!!}/deleteMsg" ><i class = 'fa fa-trash'> delete</i></a>
                                      <a href = '#' class = 'viewEdit btn btn-primary btn-xs' data-link = '/herepark/{!!$herepark->id!!}/edit'><i class = 'fa fa-edit'>Edit</i></a>
                                      <a href = '#' class = 'viewShow btn btn-warning btn-xs' data-link = '/herepark/{!!$herepark->id!!}'><i class = 'fa fa-eye'>Info</i></a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
    {!! $hereparks->render() !!}
</section>
@endsection
