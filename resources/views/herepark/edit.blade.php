@extends('scaffold-interface.layouts.appTest')
@section('title','Edit')
@section('content')

<section class="content">
    <h1>
        Editar Registro
    </h1>
    <a href="{!!url('herepark')!!}" class = 'btn btn-primary'><i class="fa fa-home"></i> Listado de Escacionamiento</a>
    <br>
    <form method = 'POST' action = '{!! url("herepark")!!}/{!!$herepark->
        id!!}/update'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Numero">Numero</label>
            <input id="Numero" name = "Numero" type="text" class="form-control" value="{!!$herepark->
            Numero!!}">
        </div>
        <div class="form-group">
            <label for="Lugar">Lugar</label>
            <input id="Lugar" name = "Lugar" type="text" class="form-control" value="{!!$herepark->
            Lugar!!}">
        </div>
        <div class="form-group">
            <label>valets Select</label>
            <select name = 'valet_id' class = "form-control">
                @foreach($valets as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>vehiculos Select</label>
            <select name = 'vehiculo_id' class = "form-control">
                @foreach($vehiculos as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <button class = 'btn btn-success' type ='submit'><i class="fa fa-floppy-o"></i> Actualizar</button>
    </form>
</section>
@endsection
