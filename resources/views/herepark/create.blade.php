@extends('scaffold-interface.layouts.appTest')
@section('title','Create')
@section('content')

<section class="content">
    <h1>
        Registar Estacionamiento
    </h1>
    <a href="{!!url('herepark')!!}" class = 'btn btn-danger'><i class="fa fa-home"></i> Listado de Estacionamiento</a>
    <br>
    <form method = 'POST' action = '{!!url("herepark")!!}'>
        <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
        <div class="form-group">
            <label for="Numero">Numero</label>
            <input id="Numero" name = "Numero" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label for="Lugar">Lugar</label>
            <input id="Lugar" name = "Lugar" type="text" class="form-control">
        </div>
        <div class="form-group">
            <label>Selecciona valet</label>
            <select name = 'valet_id' class = 'form-control'>
                @foreach($valets as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Selecciona vehiculo</label>
            <select name = 'vehiculo_id' class = 'form-control'>
                @foreach($vehiculos as $key => $value)
                <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
        </div>
        <button class = 'btn btn-success' type ='submit'> <i class="fa fa-floppy-o"></i> Guardar</button>
    </form>
</section>
@endsection
