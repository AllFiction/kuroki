<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Solicituds.
 *
 * @author  The scaffold-interface created at 2020-09-24 08:27:31pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Solicituds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('solicituds',function (Blueprint $table){

        $table->increments('id');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('vehiculo_id')->unsigned()->nullable();
        $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('solicituds');
    }
}
