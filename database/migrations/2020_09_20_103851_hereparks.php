<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Hereparks.
 *
 * @author  The scaffold-interface created at 2020-09-20 10:38:51pm
 * @link  https://github.com/amranidev/scaffold-interface
 */
class Hereparks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('hereparks',function (Blueprint $table){

        $table->increments('id');
        
        $table->String('Numero');
        
        $table->String('Lugar');
        
        /**
         * Foreignkeys section
         */
        
        $table->integer('valet_id')->unsigned()->nullable();
        $table->foreign('valet_id')->references('id')->on('valets')->onDelete('cascade');
        
        $table->integer('vehiculo_id')->unsigned()->nullable();
        $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('cascade');
        
        
        $table->timestamps();
        
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('hereparks');
    }
}
