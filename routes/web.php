<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=> 'web'],function(){
});

Route::group(['middleware'=> 'web'],function(){
});

Route::group(['middleware'=> 'web'],function(){
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware'=> 'web'],function(){
});
Route::group(['middleware'=> 'web'],function(){
});

//Rutas buscador autocomplete
Route::get('search/patentes', 'SearchController@patentes')->name('search.patentes');
Route::get('search/marcas', 'SearchController@marcas')->name('search.marcas');
//valet Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('valet','\App\Http\Controllers\ValetController');
  Route::post('valet/{id}/update','\App\Http\Controllers\ValetController@update');
  Route::get('valet/{id}/delete','\App\Http\Controllers\ValetController@destroy');
  Route::get('valet/{id}/deleteMsg','\App\Http\Controllers\ValetController@DeleteMsg');
});

//vehiculo Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('vehiculo','\App\Http\Controllers\VehiculoController');
  Route::post('vehiculo/{id}/update','\App\Http\Controllers\VehiculoController@update');
  Route::get('vehiculo/{id}/delete','\App\Http\Controllers\VehiculoController@destroy');
  Route::get('vehiculo/{id}/deleteMsg','\App\Http\Controllers\VehiculoController@DeleteMsg');
});

//herepark Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('herepark','\App\Http\Controllers\HereparkController');
  Route::post('herepark/{id}/update','\App\Http\Controllers\HereparkController@update');
  Route::get('herepark/{id}/delete','\App\Http\Controllers\HereparkController@destroy');
  Route::get('herepark/{id}/deleteMsg','\App\Http\Controllers\HereparkController@DeleteMsg');
});

Route::group(['middleware'=> 'web'],function(){
});
//solicitud Routes
Route::group(['middleware'=> 'web'],function(){
  Route::resource('solicitud','\App\Http\Controllers\SolicitudController');
  Route::post('solicitud/{id}/update','\App\Http\Controllers\SolicitudController@update');
  Route::get('solicitud/{id}/delete','\App\Http\Controllers\SolicitudController@destroy');
  Route::get('solicitud/{id}/deleteMsg','\App\Http\Controllers\SolicitudController@DeleteMsg');
});
