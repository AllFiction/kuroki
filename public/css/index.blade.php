@extends('scaffold-interface.layouts.defaultMaterialize')
@section('title','Index')
@section('content')

<div class = 'container'>
    <h1>
         Listado Parking
    </h1>
    <div class="row">
      <form class = 'col s3' method = 'get' action = '{!!url("usuario")!!}'>
          <button class = 'btn red' type = 'submit'>Listado de Clientes</button>
      </form>
      <form class = 'col s3' method = 'get' action = '{!!url("vehiculo")!!}'>
          <button class = 'btn red' type = 'submit'>Listado de Vehiculos</button>
      </form>
        <form class = 'col s3' method = 'get' action = '{!!url("test2")!!}/create'>
            <button class = 'btn red' type = 'submit'>Crear Nuevo test2</button>
        </form>
        <ul id="dropdown" class="dropdown-content">
            <li><a href="http://localhost/park/public/valet">Valet</a></li>
            <li><a href="http://localhost/park/public/usuario">Usuario</a></li>
            <li><a href="http://localhost/park/public/vehiculo">Vehiculo</a></li>
            <li><a href="http://localhost/park/public/test2">Crear Nuevo</a></li>
            <li><a href="http://localhost/park/public/solicitado">Solicitados</a></li>
            <li><a href="http://localhost/park/public/ocupado">Ocupados</a></li>
        </ul>
        <a class="col s3 btn dropdown-button #1e88e5 blue darken-1" href="#!" data-activates="dropdown">Associate<i class="mdi-navigation-arrow-drop-down right"></i></a>
    </div>
    <table>
        <thead>
            <th>Numero</th>
            <th>Fecha</th>
            <th>Estado</th>
            <th>Nombre Valet</th>
            <th>Nombre Cliente</th>
            <th>Contacto de cliente </th>
            <th>Patente Auto </th>
            <th>Comentario</th>
            <th>actions</th>
        </thead>
        <tbody>
          @foreach ($test2s as $test2)
            <tr>
                <td>{!!$test2->Numero!!}</td>
                <td>{!!$test2->Fecha!!}</td>
                <td>{!!$test2->estado->Estado!!}</td>
                <td>{!!$test2->valet->Nombre!!}</td>
                <td>{!!$test2->usuario->Nombre!!}</td>
                <td>{!!$test2->usuario->Contacto!!}</td>
                <td>{!!$test2->vehiculo->Patente!!}</td>
                <td>{!!$test2->vehiculo->Comentario!!}</td>
                <td>
                    <div class = 'row'>
                        <a href = '#' class = 'viewEdit btn-floating red' data-link = "/test2/{!!$test2->id!!}/delete" ><i class = 'material-icons'>delete</i></a>
                        <a href = '#' class = 'viewEdit btn-floating blue' data-link = '/test2/{!!$test2->id!!}/edit'><i class = 'material-icons'>edit</i></a>
                        <a href = '#' class = 'viewShow btn-floating orange' data-link = '/test2/{!!$test2->id!!}'><i class = 'material-icons'>info</i></a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $test2s->render() !!}

</div>
@endsection
